package ku.sci.cs.Appointment;

/** Juladis Paisalwongcharoen 5610450918	**/

import static org.junit.Assert.*;

import java.awt.List;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.GregorianCalendar;

import junit.framework.TestCase;

import org.junit.Test;

public class AppointmentTest {

	
	@Test
	public void testAddDescription() {
//		fail("Not yet implemented");
		Appointment appointment = new Onetime("meeting lunch", new GregorianCalendar(2015, 8, 6));
		assertEquals(appointment.getDescription(), "meeting lunch");
	}
	@Test
	public void testAddDate(){
		Appointment appointment = new Onetime("meeting Lunch", new GregorianCalendar(2015, 8, 6));
		assertEquals(appointment.getDate(), "06 09 2558");
	}
	@Test
	public void testCheckAppointmentDetails(){
		Appointment appointment = new Onetime("meeting Lunch", new GregorianCalendar(2015, 8, 6));
		assertEquals(appointment.toString(), "Date: 06 09 2558,  Description: meeting Lunch");
	}
	//check schedule appointment has appointment and appointment details
	@Test
	public void testAddAppointment1(){
		Appointment appointment = new Onetime("meeting Lunch", new GregorianCalendar(2015, 8, 6));
		AppointmentBook appointBooks = new AppointmentBook();
		appointBooks.add(appointment);
		assertNotNull(appointBooks);
		assertEquals(appointBooks.toString(), "Appointment Book\nDate: 06 09 2558,  Description: meeting Lunch");
	}
	@Test
	public void testAddAppointment2(){
		AppointmentBook appointmentBook = new AppointmentBook();
		appointmentBook.addOnetime( new GregorianCalendar(2015, 8, 6), "Meeting Lunch");
		assertNotNull(appointmentBook);
		assertEquals(appointmentBook.toString(), "Appointment Book\nDate: 06 09 2558,  Description: Meeting Lunch");
	}
	@Test
	public void testAddAppointment3(){
		ArrayList<Appointment> appintmentLists = new ArrayList<Appointment>();
		appintmentLists.add(new Onetime("Meeting Lunch", new GregorianCalendar(2015, 8, 6)));
		appintmentLists.add(new Onetime("Sent Work SE", new GregorianCalendar(2015, 8, 6)));
		AppointmentBook appointBooks = new AppointmentBook();
		appointBooks.addAll(appintmentLists);
		assertNotNull(appointBooks);
		assertEquals(appointBooks.toString(), "Appointment Book\nDate: 06 09 2558,  Description: Meeting Lunch\nDate: 06 09 2558,  Description: Sent Work SE");
	}
	@Test
	public void testGetNumAppointments(){
		Appointment appointment = new Onetime("meeting Lunch", new GregorianCalendar(2015, 8, 6));
		AppointmentBook appointBooks = new AppointmentBook();
		appointBooks.add(appointment);
		assertEquals(1,appointBooks.getNumAppointment(),0);
		
	}
	@Test
	public void testGetAppointment(){
		ArrayList<Appointment> appintmentLists = new ArrayList<Appointment>();
		appintmentLists.add(new Onetime("Meeting Lunch", new GregorianCalendar(2015, 8, 6)));
		appintmentLists.add(new Onetime("Sent Work SE", new GregorianCalendar(2015, 8, 6)));
		AppointmentBook appointBooks = new AppointmentBook();
		appointBooks.addAll(appintmentLists);
		assertEquals(appointBooks.getAppointment(2), "Date: 06 09 2558,  Description: Sent Work SE");
	}
	@Test
	public void testToStringAppointmentBook(){
		ArrayList<Appointment> appintmentLists = new ArrayList<Appointment>();
		appintmentLists.add(new Onetime("Meeting Lunch", new GregorianCalendar(2015, 8, 6)));
		appintmentLists.add(new Onetime("Sent Work SE", new GregorianCalendar(2015, 8, 6)));
		AppointmentBook appointBooks = new AppointmentBook();
		appointBooks.addAll(appintmentLists);
		assertEquals(appointBooks.toString(), "Appointment Book\nDate: 06 09 2558,  Description: Meeting Lunch\nDate: 06 09 2558,  Description: Sent Work SE");
	}
	@Test
	public void testAddOnetime(){
		AppointmentBook appointBooks = new AppointmentBook();
		appointBooks.addOnetime(new GregorianCalendar(2015, 8, 6), "Meeting Friend");
		assertEquals(appointBooks.toString(), "Appointment Book\nDate: 06 09 2558,  Description: Meeting Friend");
		assertEquals(1,appointBooks.getNumAppointment(),0);
	}
	@Test
	public void testAddDaily(){
		AppointmentBook appointBooks = new AppointmentBook();
		Calendar date = new GregorianCalendar(2015, 8, 6);
		appointBooks.addDaily(date, "Sleep");
		assertEquals(appointBooks.occursOn(date), "Appointment of 06 09 2558\nDate: 06 09 2558,  Description: Sleep");
		date = new GregorianCalendar(2015, 8, 7);
		assertEquals(appointBooks.occursOn(date), "Appointment of 07 09 2558\nDate: 07 09 2558,  Description: Sleep");

	}
	@Test
	public void testAddWeekly(){
		AppointmentBook appointBooks = new AppointmentBook();
		Calendar date = new GregorianCalendar(2015, 8, 6);
		appointBooks.addWeekly(date, "Sleep");
		assertEquals(appointBooks.occursOn(date), "Appointment of 06 09 2558\nDate: 06 09 2558,  Description: Sleep");
		date = new GregorianCalendar(2015, 8, 13);
		assertEquals(appointBooks.occursOn(date), "Appointment of 13 09 2558\nDate: 13 09 2558,  Description: Sleep");

	}
	@Test
	public void testAddMonthly(){
		AppointmentBook appointBooks = new AppointmentBook();
		Calendar date = new GregorianCalendar(2015, 8, 6);
		appointBooks.addMonthly(date, "Sleep");
		assertEquals(appointBooks.occursOn(date), "Appointment of 06 09 2558\nDate: 06 09 2558,  Description: Sleep");
		date = new GregorianCalendar(2015, 9, 6);
		assertEquals(appointBooks.occursOn(date), "Appointment of 06 10 2558\nDate: 06 10 2558,  Description: Sleep");

	}
	@Test
	public void testAddTask(){
		AppointmentBook appointBooks = new AppointmentBook();
		Calendar date = new GregorianCalendar(2015, 8, 29);
		appointBooks.addTask(date, "Do lab 471");
		assertEquals(appointBooks.occursOn(date), "Appointment of 29 09 2558\nDate: 29 09 2558,  Description: Do lab 471,  Done: false");
	}

}
