package ku.sci.cs.Appointment;

import java.text.SimpleDateFormat;
import java.util.Calendar;

/** Juladis Paisalwongcharoen 5610450918	**/

public class Task extends Appointment {
	private boolean statusFinish;
	public Task(String description, Calendar date) {
		super(description, date);
		// TODO Auto-generated constructor stub
		statusFinish = false;
	}
	public void taskFinish(){
		statusFinish = true;
		
	}
	public boolean getStatus(){
		return statusFinish;
	}
	public String toString(){
		return super.toString()+",  Done: "+statusFinish;
	}


}
