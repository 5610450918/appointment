package ku.sci.cs.Appointment;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

/** Juladis Paisalwongcharoen 5610450918	**/

public abstract class Appointment {
	private String description;
	private Calendar date;
	private SimpleDateFormat dateFormat ;
	public Appointment(String description,Calendar date) {
		this.description = description;
		this.date = date;
		dateFormat = new SimpleDateFormat("dd MM yyyy");
		
	}
	public String getDescription(){
		return description;
	}
	public String getDate(){
		return dateFormat.format(date.getTime());
	}
	public Calendar getCalendar(){
		return date;
	}
	public String toString(){
		return "Date: "+getDate()+",  Description: "+getDescription();
	}
	
}
