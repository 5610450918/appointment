package ku.sci.cs.Appointment;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Scanner;

import ku.cs.rmiexampleserver.AccountService;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

/** Juladis Paisalwongcharoen 5610450918	**/

public class AppointmentConsole implements IExtensionManager,AccountService {
	private AppointmentFrame frame;
	private FileReader fileReader;
	private String file;
	private BufferedReader in;
	private FileWriter fileWriter;
	private PrintWriter out;
	private Connection conn;
	private Statement statement;

	public static void main(String[] args) throws IOException {
		new AppointmentConsole();
		ApplicationContext  ap = new ClassPathXmlApplicationContext("Appointment.xml");
		Appointment onetimeAppointment = (Onetime)ap.getBean("appointment1");
		System.out.println(onetimeAppointment.toString());
		Appointment dailyAppointment = (Daily)ap.getBean("appointment2");
		System.out.println(dailyAppointment.toString());
		Appointment weeklyAppointment = (Weekly)ap.getBean("appointment3");
		System.out.println(weeklyAppointment.toString());
		Appointment monthlyAppointment = (Monthly)ap.getBean("appointment4");
		System.out.println(monthlyAppointment.toString());
		Task task = (Task)ap.getBean("task");
		System.out.println(task.toString());
		
	}
	public void connectGUI(){
		frame = new AppointmentFrame();
		frame.setLocation(300, 75);
		frame.setVisible(true);
//		frame.setResultsText(appointmentBook.toString());
		frame.addOkActionListener(new OkButtonActionListener());
		frame.addSaveAcionListenner(new SaveButtonActionListener());
	}
	private AppointmentBook appointmentBook;
	public AppointmentConsole() {
		try{
			Class.forName("org.sqlite.JDBC");
	 		String dbURL = "jdbc:sqlite:appointmentdb";
	 		conn = DriverManager.getConnection(dbURL);
	 		statement = conn.createStatement();
		}catch (ClassNotFoundException ex) {
	 		ex.printStackTrace();
	 	}catch (SQLException ex) {
	 		ex.printStackTrace();
	 	}
		
		file = "Appointment.txt";
		try{
//			fileWriter = new FileWriter(file);
			fileReader = new FileReader(file);
			
			in = new BufferedReader(fileReader);
//			out = new PrintWriter(fileWriter);
		}catch(IOException e){
			
		}
		appointmentBook = new AppointmentBook();
		
		frame = new AppointmentFrame();
		frame.setLocation(300, 75);
		frame.setVisible(true);
//		frame.setResultsText(appointmentBook.toString());
		frame.addOkActionListener(new OkButtonActionListener());
		frame.addSaveAcionListenner(new SaveButtonActionListener());
		
//		try {
//			readFile();
//		} catch (IOException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}
//		finally {
//			try {
//				if (fileReader != null)
//					fileReader.close();
//			}catch (IOException e) {
//				System.err.println("Error closing files");
//			}
//		}
		try {
			readSQLite();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		run();
	}
	public void run(){
		boolean status = true;
		Scanner sc = new Scanner(System.in);
		while(status){
			System.out.println("Menu\n1.Show Appointment\n2.Add Appointment\n3.Search\n4.Exit");
			System.out.println("Choose Menu");
			String menu = sc.nextLine();
			if(menu.equals("1")){
				showAppointment();
			}
			else if(menu.equals("2")){
				System.out.println("1.Add Appointment\n2.Add Task");
				String typeAppointment = sc.nextLine();
				System.out.println("Date(Day Month Year): ");
				String date = sc.nextLine();
				String[] dateDetails = date.split(" ");
//				System.out.println(dateDetails[1]);
				int day = Integer.parseInt(dateDetails[0]);
				int month = Integer.parseInt(dateDetails[1]);
				int year = Integer.parseInt(dateDetails[2]);
				Calendar cal = new GregorianCalendar(year,month-1,day);
				System.out.println("Description: ");
				String description = sc.nextLine();
				if(typeAppointment.equals("1")){
					addAppointment(cal, description);
				}
				else{
					addTask(cal, description);
				}
				
			}
			else if(menu.equals("3")){
				System.out.println("Enter Date(Day Month Year):");
				String date = sc.nextLine();
				String[] dateDetails = date.split(" ");
//				System.out.println(dateDetails[1]);
				int day = Integer.parseInt(dateDetails[0]);
				int month = Integer.parseInt(dateDetails[1]);
				int year = Integer.parseInt(dateDetails[2]);
				Calendar cal = new GregorianCalendar(year,month-1,day);
				System.out.println(Search(cal));
			}
			else{
				status = false;
			}
		}
	}
	public void showAppointment(){
		System.out.println(appointmentBook.toString()+"\nTotal Appointment: "+appointmentBook.getNumAppointment()+"\n");
		
	}
	public void addAppointment(Calendar date, String desc){
		appointmentBook.addOnetime(date, desc);
		
	}
	public void addAppointmentDaily(Calendar date, String desc){
		appointmentBook.addDaily(date, desc);
	}
	public void addAppointmentWeekly(Calendar date, String desc){
		appointmentBook.addWeekly(date, desc);
	}
	public void addAppointmentMonthly(Calendar date, String desc){
		appointmentBook.addMonthly(date, desc);
	}
	public void addTask(Calendar date, String desc){
		appointmentBook.addTask(date, desc);
	}
	public String Search(Calendar date){
		return appointmentBook.occursOn(date);
	}
	public void addAppoint(String type,Calendar date,String description){
		appointmentBook.addAppointment(type, date, description);
	}
	public class SaveButtonActionListener implements ActionListener{

		public void actionPerformed(ActionEvent e) {
			// TODO Auto-generated method stub
			String selectType = frame.getTypeAppointmentSelect();
			String description = frame.getNameText().trim();
			int day = frame.getDayNewAppointment();
			int month = frame.getMonthNewAppointment();
			int year = frame.getYearNewAppointment();
			Calendar cal = new GregorianCalendar(year,month,day);
			if(!description.equals("")){
				appointmentBook.addAppointment(selectType, cal, description);
				if(selectType.equals("Appointment")){
					String selectRadio = frame.getTypeRadioSelect();
//					if(selectRadio.equals("OneTime")){
//						addAppointment(cal, description);
//					}
//					else if(selectRadio.equals("Daily")){
//						addAppointmentDaily(cal, description);
//					}
//					else if(selectRadio.equals("Weekly")){
//						addAppointmentWeekly(cal, description);
//					}
//					else{
//						addAppointmentMonthly(cal, description);
//					}
					appointmentBook.addAppointment(selectRadio, cal, description);
					try {
						writeSQLite(description, day, month, year, "null");
					} catch (SQLException e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					}
				}
				else{
					appointmentBook.addAppointment(selectType, cal, description);	
					try {
						writeSQLite(description, day, month, year, "false");
					} catch (SQLException e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					}
				}
				String str = appointmentBook.toString();
				frame.setResultsText(str);
//				try{
//					writeFile(str.substring(str.indexOf("Date")));
//				}catch(IOException e1){
//					
//				}
				frame.showHome();

			}
			else{
				frame.showWarning();
			}
		}
		
	}
	public class OkButtonActionListener implements ActionListener{

		public void actionPerformed(ActionEvent e) {
			// TODO Auto-generated method stub
			int day = frame.getDaySearchAppointment();
			int month = frame.getMonthSearchAppointment();
			int year = frame.getYearSearchAppointment();
			Calendar cal = new GregorianCalendar(year,month,day);
			frame.setSearchText(Search(cal));
			
		}
		
	}
	public String showAllAppoint(){
		return appointmentBook.toString();
	}
	public void readFile() throws IOException {
		// TODO Auto-generated method stub
//		System.out.println("1");
		String line;
		
		for (line = in.readLine(); line != null; line = in.readLine()) {
			String[] data = line.split(", ");
			if(line.indexOf("Done")>=0){
				String date = data[0].substring(6).trim();
				String des = data[1].substring(13).trim();
				String status = data[1].substring(6).trim();
				String[] dmy = date.split(" ");
				int day = Integer.parseInt(dmy[0]);
				int month = Integer.parseInt(dmy[1]);
				int year = Integer.parseInt(dmy[2])-543;
				Calendar cal = new GregorianCalendar(year,month,day);
//				appointmentBook.addTask(cal, des);
				appointmentBook.addAppointment("Task", cal, des);
			}
			else{
				String date = data[0].substring(6).trim();
				String des = data[1].substring(13).trim();
				String[] dmy = date.split(" ");
				int day = Integer.parseInt(dmy[0]);
				int month = Integer.parseInt(dmy[1]);
				int year = Integer.parseInt(dmy[2])-543;
				Calendar cal = new GregorianCalendar(year,month,day);
//				appointmentBook.addDaily(cal, des);
				appointmentBook.addAppointment("Daily", cal, des);
			}
			
		
		}
		
		frame.setResultsText(appointmentBook.toString());
		
	}
	public void writeFile(String str) throws IOException {
		// TODO Auto-generated method stub
		fileWriter = new FileWriter(file);
		out = new PrintWriter(fileWriter);
//		System.out.println(str);
		out.println(str);
		out.flush();
		fileWriter.close();
	}
	public void readSQLite() throws SQLException {
		// TODO Auto-generated method stub
		if(conn != null){
			String query = "Select * from appointment";
			ResultSet resultSet = statement.executeQuery(query);
			while (resultSet.next()) {
				String des = resultSet.getString(1);
				int day = resultSet.getInt(2);
				int month = resultSet.getInt(3);
				int year = resultSet.getInt(4);
				String status = resultSet.getString(5);
				Calendar cal = new GregorianCalendar(year,month,day);
				if(status.equals("null")){
					addAppointment(cal, des);
				}
				else{
					addTask(cal, des);
				}
			}
		}
		frame.setResultsText(appointmentBook.toString());
		
	}
	public void writeSQLite(String des,int day,int month,int year,String status) throws SQLException {
		// TODO Auto-generated method stub
		String sql = "INSERT INTO appointment (description,day,month,year,status) " +
                "VALUES ('"+des+"',"+day+","+month+","+year+",'"+status+"' );"; 
		statement.executeUpdate(sql);
		
		
	}
	
	

}
