package ku.sci.cs.Appointment;

import java.io.IOException;
import java.sql.SQLException;

public interface IExtensionManager {
	void readFile() throws IOException;
	void writeFile(String str) throws IOException;
	void readSQLite() throws SQLException;
	void writeSQLite(String des,int day,int month,int year,String status) throws SQLException;

}
