package ku.sci.cs.Appointment;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;

import javax.swing.JDialog;
import javax.swing.JInternalFrame;
import javax.swing.JOptionPane;

/** Juladis Paisalwongcharoen 5610450918	**/

public class AppointmentBook {
	private ArrayList<Appointment> appointmentList ;

	public AppointmentBook(){
		appointmentList = new ArrayList<Appointment>();

	}
	
	public void addAll(List<Appointment> list){
		for(Appointment app : list){
			appointmentList.add(app);
		}
		
	}
	public void addAppointment(String type,Calendar date,String description){
		appointmentList.add(AppointmentFactory.getAppointment(type, date, description));
	}
	public void add(Appointment apt){
		appointmentList.add(apt);
		
	}
	public void addOnetime(Calendar date, String desc){
		appointmentList.add(new Onetime(desc,date));
		
	}
	public void addTask(Calendar date, String desc){
		appointmentList.add(new Task(desc, date));
	}
	public void addDaily(Calendar date, String desc){
		int day = date.get(Calendar.DATE);
		int month = date.get(Calendar.MONTH);
		int year = date.get(Calendar.YEAR);
		for(int i=0;i<7;i++){
			Calendar cal =  new GregorianCalendar(year,month,day+i);
//			System.out.println(day);
			appointmentList.add(new Daily(desc,cal));
		}
		
		
	}
	public void addWeekly(Calendar date, String desc){
		int day = date.get(Calendar.DATE);
		int month = date.get(Calendar.MONTH);
		int year = date.get(Calendar.YEAR);
		for(int i=0;i<7;i++){
			Calendar cal =  new GregorianCalendar(year,month,day+(7*i));
//			System.out.println(day);
			appointmentList.add(new Daily(desc,cal));
		}
		
		
	}
	public void addMonthly(Calendar date, String desc){
		int day = date.get(Calendar.DATE);
		int month = date.get(Calendar.MONTH);
		int year = date.get(Calendar.YEAR);
		for(int i=0;i<7;i++){
			Calendar cal =  new GregorianCalendar(year,month+i,day);
//			System.out.println(day);
			appointmentList.add(new Daily(desc,cal));
		}
		
		
	}
	public Integer getNumAppointment(){
		return appointmentList.size();
		
	}
	public String getAppointment(int index){
		Appointment app = appointmentList.get(index-1);
		return app.toString();
	}
	public String toString(){
		String str = "Appointment Book";
		for(Appointment app : appointmentList){
			str += "\n"+app.toString();
		}
		return str;
	}
	public String occursOn(Calendar date){
		SimpleDateFormat dateFormat = new SimpleDateFormat("dd MM yyyy");
		String strDate = dateFormat.format(date.getTime());
		String str = "Appointment of "+strDate;
		
		for(Appointment app : appointmentList){
//			System.out.println(strDate+" "+app.getDate());
			if(strDate.equals(app.getDate())){
				str += "\n"+app.toString();
			}
		}
		return str;
	}
	
	
	

}
