package ku.sci.cs.Appointment;

import java.awt.BorderLayout;
import java.awt.CardLayout;
import java.awt.Dialog;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.ButtonGroup;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JInternalFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.border.TitledBorder;

public class AppointmentFrame extends JFrame {
	private JTextArea results;
	private JPanel cardPanel;
	private CardLayout card;
	private JPanel newPanel;
	private JPanel showPanel;
	private JButton backButton;
	private JTextField nameText;
	private JComboBox<String> appointmentComboBox;
	private JButton cancelButton;
	private JButton saveButton;
	private JPanel typeAppointPanel;
	private JComboBox<String> yearAppointCombo;
	private JComboBox<String> monthAppointCombo;
	private JComboBox<String> dayAppointCombo;
	private JRadioButton onetimeRadio;
	private JRadioButton dailyRadio;
	private JRadioButton weeklyRadio;
	private JRadioButton monthlyRadio;
	private JPanel typeAppointmentPanel;
	private JComboBox<String> daySearchCombo;
	private JComboBox<String> monthSearchCombo;
	private JComboBox<String> yearSearchCombo;
	private JTextArea resultsSearch;
	private JButton okButton;
	public AppointmentFrame(){
		cardPanel = new JPanel();
		card = new CardLayout();
		cardPanel.setLayout(card);
		createMenu();
		createSearch();
		createNew();
	}
	public void createMenu(){
		setSize(700, 450);
		JLabel titlelabel = new JLabel("APPOINTMENT");
		JPanel titlePanel = new JPanel();
		titlePanel.add(titlelabel);
		
		JButton homeButton = new JButton("HOME");
		JButton searchButton = new JButton("SEARCH");
		JButton newButton = new JButton("NEW");
		
		
		results = new JTextArea(21,60);
		JScrollPane resultsScroll = new JScrollPane(results);
		showPanel = new JPanel();
		showPanel.add(homeButton);
		showPanel.add(searchButton);
		showPanel.add(newButton);
		showPanel.add(resultsScroll);
		
		cardPanel.add(showPanel,"Home");
		
		add(titlePanel,BorderLayout.NORTH);
		add(cardPanel,BorderLayout.CENTER);
		
		addHomeActionListener(homeButton);
		addSearchActionListener(searchButton);
		addNewActionListener(newButton);
		
		results.setEditable(false);
		
		
	}
	public void createSearch(){
		JPanel searchPanel = new JPanel();
		cardPanel.add(searchPanel,"SearchPanel");
		JButton homeButton = new JButton("HOME");
		JButton searchButton = new JButton("SEARCH");
		okButton = new JButton("OK");
		
		resultsSearch = new JTextArea(19,60);
		resultsSearch.append("Choose Date");
		JScrollPane resultsScroll = new JScrollPane(resultsSearch);
		
		daySearchCombo = new JComboBox<String>();
		monthSearchCombo = new JComboBox<String>();
		yearSearchCombo = new JComboBox<String>();
		JPanel dateSearchPanel = new JPanel();
		dateSearchPanel.setBorder(new TitledBorder("Date Chooser"));
		dateSearchPanel.add(daySearchCombo);
		dateSearchPanel.add(monthSearchCombo);
		dateSearchPanel.add(yearSearchCombo);
		
		searchPanel.add(homeButton);
		searchPanel.add(searchButton);
		searchPanel.add(dateSearchPanel);
		searchPanel.add(okButton);
		searchPanel.add(resultsScroll);
		
		addHomeActionListener(homeButton);
		addSearchActionListener(searchButton);

		setDateComboBox(daySearchCombo, monthSearchCombo, yearSearchCombo);
		
		resultsSearch.setEditable(false);
		
	}
	public void createNew(){
		newPanel = new JPanel();
		newPanel.setLayout(new BorderLayout());
		appointmentComboBox = new JComboBox<String>();
		JPanel comboPanel = new JPanel();
		appointmentComboBox.addItem("Choose Type");
		appointmentComboBox.addItem("Appointment");
		appointmentComboBox.addItem("Task");
		comboPanel.add(appointmentComboBox);
		newPanel.add(comboPanel, BorderLayout.NORTH);
		
		typeAppointPanel = new JPanel();
		typeAppointPanel.setLayout(card);
		
		JPanel choosePanel = new JPanel();
		choosePanel.setLayout(new BorderLayout());
		JPanel appointPanel = new JPanel();
		dayAppointCombo = new JComboBox<String>();
		monthAppointCombo = new JComboBox<String>();
		yearAppointCombo = new JComboBox<String>();
		
		
		typeAppointmentPanel = new JPanel();
		typeAppointmentPanel.setBorder(new TitledBorder("Type Appointment"));
		onetimeRadio = new JRadioButton("OneTime");
		dailyRadio = new JRadioButton("Daily");
		weeklyRadio = new JRadioButton("Weekly");
		monthlyRadio = new JRadioButton("Monthly");
		ButtonGroup typeAppointRadio = new ButtonGroup();
		typeAppointRadio.add(onetimeRadio);
		typeAppointRadio.add(dailyRadio);
		typeAppointRadio.add(weeklyRadio);
		typeAppointRadio.add(monthlyRadio);
		
		typeAppointmentPanel.add(onetimeRadio);
		typeAppointmentPanel.add(dailyRadio);
		typeAppointmentPanel.add(weeklyRadio);
		typeAppointmentPanel.add(monthlyRadio);
		appointPanel.add(typeAppointmentPanel);
		
		
		JPanel dateAppointPanel = new JPanel();
		dateAppointPanel.setBorder(new TitledBorder("Date Chooser"));
		dateAppointPanel.add(dayAppointCombo);
		dateAppointPanel.add(monthAppointCombo);
		dateAppointPanel.add(yearAppointCombo);
		appointPanel.add(dateAppointPanel);
		
		JPanel registerAppointPanel = new JPanel();
		JLabel descriptionLabel = new JLabel("Description");
		nameText = new JTextField(10);
		registerAppointPanel.add(descriptionLabel);
		registerAppointPanel.add(nameText);
		appointPanel.add(registerAppointPanel);
		
		cancelButton = new JButton("CANCEL");
		saveButton = new JButton("SAVE");
		
		JPanel buttonPanel = new JPanel();
		buttonPanel.add(cancelButton);
		buttonPanel.add(saveButton);
		
		typeAppointPanel.add(choosePanel, "ChoosePanel");
		typeAppointPanel.add(appointPanel, "AppointPanel");
		
		newPanel.add(typeAppointPanel, BorderLayout.CENTER);
		newPanel.add(buttonPanel, BorderLayout.SOUTH);
		cardPanel.add(newPanel,"NewPanel");
		
		saveButton.setVisible(false);
		
		setDateComboBox(dayAppointCombo,monthAppointCombo,yearAppointCombo);
		addAppointmentActionListener();
		addCancelActionListener();

		
		
	}
	private void setDateComboBox(final JComboBox<String> day,final JComboBox<String> month,JComboBox<String> year){
//		day.addItem("Day");
//		month.addItem("Month");
//		year.addItem("Year");
		for(int i=0;i<=5;i++){
			year.addItem((2015+i)+"");
		}
		month.addItem("January");
		month.addItem("February");
		month.addItem("March");
		month.addItem("April");
		month.addItem("May");
		month.addItem("June");
		month.addItem("July");
		month.addItem("August");
		month.addItem("September");
		month.addItem("October");
		month.addItem("November");
		month.addItem("December");
		month.addActionListener(new ActionListener() {
			
			public void actionPerformed(ActionEvent e) {
				// TODO Auto-generated method stub
				int index = month.getSelectedIndex();
				int indexDay = day.getSelectedIndex();
				day.removeAllItems();
				if(index == 0 || index ==2 || index ==4 || index ==6 || index ==7 || index ==9 || index ==11){
					for(int i=1;i<=31;i++){
						day.addItem(i+"");
					}
				}
				else if(index ==3 || index ==5 || index ==8 || index ==10){
					for(int i=1;i<=30;i++){
						day.addItem(i+"");
					}
				}
				else{
					for(int i=1;i<=28;i++){
						day.addItem(i+"");
					}
				}
				day.setSelectedIndex(indexDay);
			}
		});
		
		
	}
	public void addCancelActionListener(){
		cancelButton.addActionListener(new ActionListener() {
			
			public void actionPerformed(ActionEvent e) {
				// TODO Auto-generated method stub
				showHome();
			}
		});
	}
	public void addAppointmentActionListener(){
		appointmentComboBox.addActionListener(new ActionListener() {
			
			public void actionPerformed(ActionEvent e) {
				// TODO Auto-generated method stub
				String selectItem = (String) appointmentComboBox.getSelectedItem();
				if(selectItem.equals("Choose Type")){
					card.show(typeAppointPanel, "ChoosePanel");
					saveButton.setVisible(false);
				}
				else if(selectItem.equals("Appointment")){
					card.show(typeAppointPanel, "AppointPanel");
					typeAppointmentPanel.setVisible(true);
					saveButton.setVisible(true);

					monthAppointCombo.setSelectedIndex(0);
					dayAppointCombo.setSelectedIndex(0);
					yearAppointCombo.setSelectedIndex(0);
					onetimeRadio.setSelected(true);
					nameText.setText("");
				}
				else{
					card.show(typeAppointPanel, "AppointPanel");
					typeAppointmentPanel.setVisible(false);
					saveButton.setVisible(true);
					
					monthAppointCombo.setSelectedIndex(0);
					dayAppointCombo.setSelectedIndex(0);
					yearAppointCombo.setSelectedIndex(0);
					nameText.setText("");
				}
				
			}
		});
	}
	public void addBackActionListener(){
		backButton.addActionListener(new ActionListener() {
			
			public void actionPerformed(ActionEvent e) {
				// TODO Auto-generated method stub
				card.previous(cardPanel);
			}
		});
	}
	public void addNewActionListener(JButton button){
		button.addActionListener(new ActionListener() {
			
			public void actionPerformed(ActionEvent e) {
				// TODO Auto-generated method stub
//				setSize(300, 450);
//				createNew();
				setSize(350, 450);
				card.show(cardPanel, "NewPanel");
				appointmentComboBox.setSelectedIndex(0);
				monthAppointCombo.setSelectedIndex(0);
				dayAppointCombo.setSelectedIndex(0);
				yearAppointCombo.setSelectedIndex(0);
				onetimeRadio.setSelected(true);
				nameText.setText("");
				
			}
		});
	}
	public void addHomeActionListener(JButton button){
		button.addActionListener(new ActionListener() {
			
			public void actionPerformed(ActionEvent e) {
				// TODO Auto-generated method stub
				showHome();
			}
		});
	}
	public void addSearchActionListener(JButton button){
		button.addActionListener(new ActionListener() {
			
			public void actionPerformed(ActionEvent e) {
				// TODO Auto-generated method stub
				showSearch();
			}
		});
	}
	public void addSaveAcionListenner(ActionListener list){
		saveButton.addActionListener(list);
	}
	public String getNameText(){
		return nameText.getText();
	}
	public String getTypeAppointmentSelect(){
		String selectItem = (String) appointmentComboBox.getSelectedItem(); 
		return selectItem;
	}
	public void setResultsText(String str){
		results.setText(str);
	}
	public String getTypeRadioSelect(){
		if(onetimeRadio.isSelected()){
			return "OneTime";
		}
		else if(dailyRadio.isSelected()){
			return "Daily";
		}
		else if(weeklyRadio.isSelected()){
			return "Weekly";
		}
		else{
			return "Monthly";
		}
	}
	public int getYearNewAppointment(){
		String str = (String) yearAppointCombo.getSelectedItem();
		int year = Integer.parseInt(str);
		return year;
	}
	public int getMonthNewAppointment(){
		int index = monthAppointCombo.getSelectedIndex();
		return index;
	}
	public int getDayNewAppointment(){
		int index = dayAppointCombo.getSelectedIndex();
		return index+1;
	}
	public void showWarning(){
//		JInternalFrame frame = new JInternalFrame();
		JOptionPane.showMessageDialog(this, "Add Description");
	}
	public void showHome(){
		setSize(700, 450);
		card.show(cardPanel, "Home");
	}
	public void showSearch(){
		setSize(700, 450);
		card.show(cardPanel, "SearchPanel");
		monthSearchCombo.setSelectedIndex(0);
		daySearchCombo.setSelectedIndex(0);
		yearSearchCombo.setSelectedIndex(0);
	}
	public void addOkActionListener(ActionListener list){
		okButton.addActionListener(list);
	}
	public int getYearSearchAppointment(){
		String str = (String) yearSearchCombo.getSelectedItem();
		int year = Integer.parseInt(str);
		return year;
	}
	public int getMonthSearchAppointment(){
		int index = monthSearchCombo.getSelectedIndex();
		return index;
	}
	public int getDaySearchAppointment(){
		int index = daySearchCombo.getSelectedIndex();
		return index+1;
	}
	public void setSearchText(String str){
		resultsSearch.setText(str);
	}

}
