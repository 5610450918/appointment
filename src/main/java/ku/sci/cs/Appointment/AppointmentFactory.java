package ku.sci.cs.Appointment;

import java.util.Calendar;

public class AppointmentFactory {
	public static Appointment getAppointment(String type,Calendar date,String description){
		if("Onetime".equals(type)){
			return new Onetime(description, date);
		}
		else if("Daily".equals(type)){
			return new Daily(description, date);
		}
		else if("Weekly".equals(type)){
			return new Weekly(description, date);
		}
		else if("Monthly".equals(type)){
			return new Monthly(description, date);
		}
		else if("Task".equals(type)){
			return new Task(description, date);
		}
		else{
			return null;
		}
	}

}
