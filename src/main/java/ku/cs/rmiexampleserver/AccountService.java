package ku.cs.rmiexampleserver;

import java.util.Calendar;

public interface AccountService {
	public void addAppoint(String type,Calendar date,String description);
    public String showAllAppoint();
}
