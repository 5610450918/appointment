package ku.cs.rmiexampleclient;

import java.util.Calendar;
import java.util.GregorianCalendar;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class ClientMain {
	public static void main(String[] args) {

		ApplicationContext bf =  
				new ClassPathXmlApplicationContext("client-config.xml");

		AccountService service = (AccountService) bf.getBean("acctService");
		Calendar cal = new GregorianCalendar(2015,11,3);
		service.addAppoint("Onetime", cal, "lab se");
		System.out.println(service.showAllAppoint());
		service.connectGUI();
//		service.insertAccount("Kwan");
//		System.out.println(service.getAccount("Kwan"));

	}
}
