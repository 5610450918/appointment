package ku.cs.rmiexampleclient;

import java.util.Calendar;

public interface AccountService {
    public void addAppoint(String type,Calendar date,String description);
    public String showAllAppoint();
    public void connectGUI();
}
